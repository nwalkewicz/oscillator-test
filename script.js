'use strict';
const inpBPM = document.getElementById('inpBPM');
const noteModifier = document.getElementById('noteModifier');

let audioContext;
let oscillator;

const notes = {
  af: [       -1,   51.9131,  103.826 ,  207.652,  415.305,  830.609, 1661.22],
  a:  [  27.5000,   55.0000,  110.000 ,  220.000,  440.000,  880.000, 1760.00],
  bf: [  29.1352,   58.2705,  116.541 ,  233.082,  466.164,  932.328, 1864.66],
  b:  [  30.8677,   61.7354,  123.471 ,  246.942,  493.883,  987.767, 1975.53],
  c:  [       -1,   32.7032,   65.4064,  130.813,  261.626,  523.251, 1046.50],
  df: [       -1,   34.6478,   69.2957,  138.591,  277.183,  554.365, 1108.73],
  d:  [       -1,   36.7081,   73.4162,  146.832,  293.665,  587.330, 1174.66],
  ef: [       -1,   38.8909,   77.7817,  155.563,  311.127,  622.254, 1244.51],
  e:  [       -1,   41.2034,   82.4069,  164.814,  329.628,  659.255, 1318.51],
  f:  [       -1,   43.6535,   87.3071,  174.614,  349.228,  698.456, 1396.91],
  gf: [       -1,   46.2493,   92.4986,  184.997,  369.994,  739.989, 1479.98],
  g:  [       -1,   48.9994,   97.9989,  195.998,  391.995,  783.991, 1567.98]
}

/**
 * Create a new note.
 * @param {AudioContext} audioContext Audio context
 * @param {number} frequency Frequency in hertz
 * @param {number} duration Duration of note in milliseconds
 * @param {'custom' | 'sawtooth' | 'sine' | 'square' | 'triangle'} type Type of wave
 * @returns {Promise<OscillatorNode>} Instance of OscillatorNode
 */
async function createNote(audioContext, frequency, duration = -1, type = 'sine') {
  const oscillator = new OscillatorNode(audioContext, {
    frequency,
    type
  });

  oscillator.connect(audioContext.destination);
  oscillator.start();

  if (duration !== -1) return new Promise((resolve) => {
    setTimeout(() => {
      oscillator.stop();
      resolve();
    }, duration);
  });

  return oscillator;
}

document.addEventListener('click', () => {
  // Create web audio API context
  if (!audioContext) audioContext = new AudioContext({
    latencyHint: 'interactive'
  });
});

document.addEventListener('keydown', (e) => {
  // Create web audio API context
  if (!audioContext) audioContext = new AudioContext({
    latencyHint: 'interactive'
  });

  if (e.ctrlKey || e.altKey) return;
  if (e.target != document.body) return;

  const octave = parseInt(inpOctave.value);
  let frequency;
  switch (e.key) {
    // LOWER
    case 'z': {
      frequency = notes.c[octave];
      break;
    }
    
    case 's': {
      frequency = notes.df[octave];
      break;
    }
    
    case 'x': {
      frequency = notes.d[octave];
      break;
    }
    
    case 'd': {
      frequency = notes.ef[octave];
      break;
    }
    
    case 'c': {
      frequency = notes.e[octave];
      break;
    }
    
    case 'v': {
      frequency = notes.f[octave];
      break;
    }
    
    case 'g': {
      frequency = notes.gf[octave];
      break;
    }
    
    case 'b': {
      frequency = notes.g[octave];
      break;
    }
    
    case 'h': {
      frequency = notes.af[octave];
      break;
    }
    
    case 'n': {
      frequency = notes.a[octave];
      break;
    }
    
    case 'j': {
      frequency = notes.bf[octave];
      break;
    }
    
    case 'm': {
      frequency = notes.b[octave];
      break;
    }
    
    case ',': {
      frequency = notes.c[octave + 1];
      break;
    }
    
    case 'l': {
      frequency = notes.df[octave + 1];
      break;
    }
    
    case '.': {
      frequency = notes.d[octave + 1];
      break;
    }
    
    case ';': {
      frequency = notes.ef[octave + 1];
      break;
    }
    
    case '/': {
      frequency = notes.e[octave + 1];
      break;
    }
    
    // UPPER
    case 'q': {
      frequency = notes.c[octave + 1];
      break;
    }
    
    case '2': {
      frequency = notes.df[octave + 1];
      break;
    }
    
    case 'w': {
      frequency = notes.d[octave + 1];
      break;
    }
    
    case '3': {
      frequency = notes.ef[octave + 1];
      break;
    }
    
    case 'e': {
      frequency = notes.e[octave + 1];
      break;
    }
    
    case 'r': {
      frequency = notes.f[octave + 1];
      break;
    }
    
    case '5': {
      frequency = notes.gf[octave + 1];
      break;
    }
    
    case 't': {
      frequency = notes.g[octave + 1];
      break;
    }
    
    case '6': {
      frequency = notes.af[octave + 1];
      break;
    }
    
    case 'y': {
      frequency = notes.a[octave + 1];
      break;
    }
    
    case '7': {
      frequency = notes.bf[octave + 1];
      break;
    }
    
    case 'u': {
      frequency = notes.b[octave + 1];
      break;
    }
    
    case 'i': {
      frequency = notes.c[octave + 2];
      break;
    }
    
    case '9': {
      frequency = notes.df[octave + 2];
      break;
    }
    
    case 'o': {
      frequency = notes.d[octave + 2];
      break;
    }
    
    case '0': {
      frequency = notes.ef[octave + 2];
      break;
    }
    
    case 'p': {
      frequency = notes.e[octave + 2];
      break;
    }
    
    case '[': {
      frequency = notes.f[octave + 2];
      break;
    }
    
    case '=': {
      frequency = notes.gf[octave + 2];
      break;
    }
    
    case ']': {
      frequency = notes.g[octave + 2];
      break;
    }
    
    case 'Backspace': {
      frequency = notes.af[octave + 2];
      break;
    }
    
    case '\\': {
      frequency = notes.a[octave + 2];
      break;
    }

    default: return;
  }

  e.preventDefault();

  const duration = 60 * 1000 / parseFloat(inpBPM.value) * 4 / parseInt(noteModifier.value);
  createNote(audioContext, frequency, duration, 'sawtooth');
});

document.addEventListener('keyup', (e) => {
  // console.log(e.key);
});
